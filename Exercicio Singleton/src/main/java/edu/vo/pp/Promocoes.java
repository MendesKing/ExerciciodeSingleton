package edu.vo.pp;

import edu.vo.vd.Venda;

public class Promocoes extends Venda{
	
	private int dia;
	
	private  Promocoes doSolicitaTicket = null;

	public Promocoes getDoSolicitaTicket() {
		return doSolicitaTicket;
	}

	public void setDoSolicitaTicket(Promocoes doSolicitaTicket) {
		this.doSolicitaTicket = doSolicitaTicket;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}
	
	

}
