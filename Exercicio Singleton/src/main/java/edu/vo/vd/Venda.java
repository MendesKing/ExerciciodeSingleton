package edu.vo.vd;



public class Venda {
	
	private double valor;
	
	private Venda doSolicitaTicket = null;

	public Venda getDoSolicitaTicket() {
		return doSolicitaTicket;
	}

	public void setDoSolicitaTicket(Venda doSolicitaTicket) {
		this.doSolicitaTicket = doSolicitaTicket;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	
}
