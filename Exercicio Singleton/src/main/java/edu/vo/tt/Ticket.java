package edu.vo.tt;

import edu.vo.pp.Promocoes;

public class Ticket extends Promocoes{
	
	private int sequencia;
	
	
	public int getSequencia() {
		return sequencia;
	}

	public void setSequencia(int sequencia) {
		this.sequencia = sequencia;
	}

	private static Ticket doEmiteTicket = null;
	
	
	public static Ticket getdoEmiteTicket(){
		if(doEmiteTicket  == null) {
			System.out.println("Metodo Solicitado com Sucesso ");
			doEmiteTicket = new Ticket();
		}
		return doEmiteTicket;
	}

}
